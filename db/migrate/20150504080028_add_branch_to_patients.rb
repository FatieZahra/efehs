class AddBranchToPatients < ActiveRecord::Migration
  def change
    add_reference :patients, :branch, index: true
    add_foreign_key :patients, :branches
  end
end

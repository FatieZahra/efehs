class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :item
      t.float :amount
      t.integer :actable_id
      t.string :actable_type

      t.timestamps null: false
    end
  end
end

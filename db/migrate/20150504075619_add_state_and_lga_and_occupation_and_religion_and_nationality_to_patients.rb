class AddStateAndLgaAndOccupationAndReligionAndNationalityToPatients < ActiveRecord::Migration
  def change
    add_reference :patients, :state, index: true
    add_foreign_key :patients, :states
    add_reference :patients, :lga, index: true
    add_foreign_key :patients, :lgas
    add_reference :patients, :occupation, index: true
    add_foreign_key :patients, :occupations
    add_reference :patients, :religion, index: true
    add_foreign_key :patients, :religions
    add_reference :patients, :nationality, index: true
    add_foreign_key :patients, :nationalities
  end
end

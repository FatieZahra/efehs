class AddActableIdAndActableTypeToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :actable_id, :integer
    add_column :patients, :actable_type, :string
  end
end

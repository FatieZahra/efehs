class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :surname
      t.string :first_name
      t.string :middle_name
      t.string :gender
      t.date :dob
      t.string :phone
      t.string :email
      t.string :address
      t.string :next_of_kin
      t.string :next_of_kin_phone

      t.timestamps null: false
    end
  end
end

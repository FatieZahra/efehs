require 'test_helper'

class IkejaPatientsControllerTest < ActionController::TestCase
  setup do
    @ikeja_patient = ikeja_patients(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ikeja_patients)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ikeja_patient" do
    assert_difference('IkejaPatient.count') do
      post :create, ikeja_patient: {  }
    end

    assert_redirected_to ikeja_patient_path(assigns(:ikeja_patient))
  end

  test "should show ikeja_patient" do
    get :show, id: @ikeja_patient
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ikeja_patient
    assert_response :success
  end

  test "should update ikeja_patient" do
    patch :update, id: @ikeja_patient, ikeja_patient: {  }
    assert_redirected_to ikeja_patient_path(assigns(:ikeja_patient))
  end

  test "should destroy ikeja_patient" do
    assert_difference('IkejaPatient.count', -1) do
      delete :destroy, id: @ikeja_patient
    end

    assert_redirected_to ikeja_patients_path
  end
end

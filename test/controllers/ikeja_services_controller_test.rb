require 'test_helper'

class IkejaServicesControllerTest < ActionController::TestCase
  setup do
    @ikeja_service = ikeja_services(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ikeja_services)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ikeja_service" do
    assert_difference('IkejaService.count') do
      post :create, ikeja_service: {  }
    end

    assert_redirected_to ikeja_service_path(assigns(:ikeja_service))
  end

  test "should show ikeja_service" do
    get :show, id: @ikeja_service
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ikeja_service
    assert_response :success
  end

  test "should update ikeja_service" do
    patch :update, id: @ikeja_service, ikeja_service: {  }
    assert_redirected_to ikeja_service_path(assigns(:ikeja_service))
  end

  test "should destroy ikeja_service" do
    assert_difference('IkejaService.count', -1) do
      delete :destroy, id: @ikeja_service
    end

    assert_redirected_to ikeja_services_path
  end
end

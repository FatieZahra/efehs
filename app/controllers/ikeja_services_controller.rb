class IkejaServicesController < ApplicationController
  before_action :set_ikeja_service, only: [:show, :edit, :update, :destroy]

  # GET /ikeja_services
  # GET /ikeja_services.json
  def index
    @ikeja_services = IkejaService.all
  end

  # GET /ikeja_services/1
  # GET /ikeja_services/1.json
  def show
  end

  # GET /ikeja_services/new
  def new
    @ikeja_service = IkejaService.new
  end

  # GET /ikeja_services/1/edit
  def edit
  end

  # POST /ikeja_services
  # POST /ikeja_services.json
  def create
    @ikeja_service = IkejaService.new(ikeja_service_params)

    respond_to do |format|
      if @ikeja_service.save
        format.html { redirect_to ikeja_services_path, notice: 'Ikeja service was successfully created.' }
        format.json { render :index, status: :created, location: ikeja_services_path }
      else
        format.html { render :new }
        format.json { render json: @ikeja_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ikeja_services/1
  # PATCH/PUT /ikeja_services/1.json
  def update
    respond_to do |format|
      if @ikeja_service.update(ikeja_service_params)
        format.html { redirect_to ikeja_services_path, notice: 'Ikeja service was successfully updated.' }
        format.json { render :index, status: :ok, location: ikeja_services_path }
      else
        format.html { render :edit }
        format.json { render json: @ikeja_service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ikeja_services/1
  # DELETE /ikeja_services/1.json
  def destroy
    @ikeja_service.destroy
    respond_to do |format|
      format.html { redirect_to ikeja_services_url, notice: 'Ikeja service was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ikeja_service
      @ikeja_service = IkejaService.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ikeja_service_params
      params.require(:ikeja_service).permit(:item, :amount, :category_id)
    end
end

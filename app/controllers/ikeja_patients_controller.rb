class IkejaPatientsController < ApplicationController
  before_action :set_ikeja_patient, only: [:show, :edit, :update, :destroy]

  # GET /ikeja_patients
  # GET /ikeja_patients.json
  def index
    @ikeja_patients = IkejaPatient.all
  end

  # GET /ikeja_patients/1
  # GET /ikeja_patients/1.json
  def show
  end

  # GET /ikeja_patients/new
  def new
    @ikeja_patient = IkejaPatient.new
  end

  # GET /ikeja_patients/1/edit
  def edit
  end

  # POST /ikeja_patients
  # POST /ikeja_patients.json
  def create
    @ikeja_patient = IkejaPatient.new(ikeja_patient_params)

    respond_to do |format|
      if @ikeja_patient.save
        format.html { redirect_to ikeja_patients_path, notice: 'Ikeja patient was successfully created.' }
        format.json { render :index, status: :created, location: ikeja_patients_path }
      else
        format.html { render :new }
        format.json { render json: @ikeja_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ikeja_patients/1
  # PATCH/PUT /ikeja_patients/1.json
  def update
    respond_to do |format|
      if @ikeja_patient.update(ikeja_patient_params)
        format.html { redirect_to @ikeja_patient, notice: 'Ikeja patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @ikeja_patient }
      else
        format.html { render :edit }
        format.json { render json: @ikeja_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ikeja_patients/1
  # DELETE /ikeja_patients/1.json
  def destroy
    @ikeja_patient.destroy
    respond_to do |format|
      format.html { redirect_to ikeja_patients_url, notice: 'Ikeja patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ikeja_patient
      @ikeja_patient = IkejaPatient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ikeja_patient_params
      params.require(:ikeja_patient).permit(:branch_id, :mrn, :surname, :first_name, :middle_name, :gender, :dob, :next_of_kin, :next_of_kin_phone, :state_id, :religion_id, :nationality_id, :occupation_id, :lga_id, :phone, :email, :address)
    end
end

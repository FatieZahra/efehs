class Service < ActiveRecord::Base
	actable
	validates :item, :amount, :category_id, presence: true
	validates :item, uniqueness: true
	belongs_to :category
end

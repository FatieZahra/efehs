class Branch < ActiveRecord::Base
	validates :name, :address, :prefix, :suffix, presence: true
	validates :name, :address, uniqueness: true
	has_many :patients
end

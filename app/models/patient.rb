class Patient < ActiveRecord::Base
	after_create :set_mrn
	actable
	belongs_to :state
	belongs_to :branch
	belongs_to :lga
	belongs_to :religion
	belongs_to :occupation
	belongs_to :nationality
	Gender = %w(Female Male)
	validates :surname, :first_name, :address, :gender, :phone, :state, presence: true

	def set_mrn
		prefix = self.branch.prefix
		suffix = self.branch.suffix
		update_attribute(:mrn, "#{prefix}"+"#{self.id}"+"#{suffix}")
	end

	def fullName
		self.first_name+" "+self.middle_name+" "+self.surname
	end
end

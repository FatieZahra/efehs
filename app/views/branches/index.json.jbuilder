json.array!(@branches) do |branch|
  json.extract! branch, :id, :name, :address, :prefix, :suffix
  json.url branch_url(branch, format: :json)
end

json.array!(@ikeja_services) do |ikeja_service|
  json.extract! ikeja_service, :id
  json.url ikeja_service_url(ikeja_service, format: :json)
end

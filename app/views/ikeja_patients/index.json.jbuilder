json.array!(@ikeja_patients) do |ikeja_patient|
  json.extract! ikeja_patient, :id
  json.url ikeja_patient_url(ikeja_patient, format: :json)
end

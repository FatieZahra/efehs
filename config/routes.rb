Rails.application.routes.draw do
  resources :ikeja_services

  resources :ikeja_patients

  resources :nationalities

  resources :religions

  resources :occupations

  resources :lgas

  resources :states

  resources :branches

  get 'pages/index'

  root 'pages#index'
end
